/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2it.recrutamento;

import java.util.Scanner;

/**
 *
 * @author Gustavo
 */
public class Exercicio8_ValorC {

    public static int calcularC(Integer a, Integer b) {

        char[] cA = Integer.toString(a).toCharArray();
        char[] cB = Integer.toString(b).toCharArray();
        String sC = "";

        // Verificando qual String possui mais caracters
        int maiorQtdCarac = 0;
        if (cA.length > cB.length) {
            maiorQtdCarac = cA.length;
        } else {
            maiorQtdCarac = cB.length;
        }

        for (int i = 0; i <= maiorQtdCarac; i++) {
            // Controle para nao acessar uma posicao inxistente no vertor Char
            if (cA.length > i) {
                sC += String.valueOf(cA[i]);
            }
            if (cB.length > i) {
                sC += String.valueOf(cB[i]);
            }
        }


        Integer iC = Integer.valueOf(sC);

        if (iC > 1000000) {
            return -1;
        } else {
            return iC;
        }

    }

}
