/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2it.recrutamento;

/**
 *
 * @author Gustavo
 */
public class ArvoreBinaria {
    
        private int value;
	private ArvoreBinaria dir;
	private ArvoreBinaria esq;
	
	
	public ArvoreBinaria(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	public ArvoreBinaria getRigth() {
		return dir;
	}
	public ArvoreBinaria getLeft() {
		return esq;
	}
	
	public void noDireto(final ArvoreBinaria binaryTree)
	{
		this.dir = binaryTree;
	}
	
	public void noEsquerdo(final ArvoreBinaria binaryTree)
	{
		this.esq = binaryTree;
	}
	
	public int sum(final ArvoreBinaria binaryTree)
	{
		
		if(binaryTree == null)
			return 0;
		
		final int valueLeft = sum(binaryTree.getLeft());
		final int valueRight = sum(binaryTree.getRigth());
		
		return binaryTree.value + valueLeft + valueRight;
	}
	
	
    
}
