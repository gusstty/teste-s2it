/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2it.recrutamento;

import java.util.Scanner;

/**
 *
 * @author Gustavo
 */
public class S2iTRecrutamento {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        
        ArvoreBinaria ab = new ArvoreBinaria(100);
        ArvoreBinaria ab2 = new ArvoreBinaria(200);
        ArvoreBinaria ab3 = new ArvoreBinaria(50);
        ArvoreBinaria ab4 = new ArvoreBinaria(300);

        ab.noDireto(ab2);
        ab.noEsquerdo(ab3);
        ab2.noDireto(ab4);

        int value = ab.sum(ab);
        System.out.println(value);
         
        //Teste Exercicio 8
        Scanner scan = new Scanner(System.in);
        Exercicio8_ValorC valorC = new Exercicio8_ValorC();
        System.out.println("Entre com o Valor de A: ");
        int a = scan.nextInt();
        System.out.println("Entre com o Valor de B: ");
        int b = scan.nextInt();
        System.out.println(valorC.calcularC(a, b));
        
        
        scan.close();
         
    }
    
}
